package com.onready.pdf.domain;

import lombok.Data;

@Data
public class Receipt {

  private String company;
  private String companyCuit;

}
